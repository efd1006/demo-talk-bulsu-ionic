import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.page.html',
  styleUrls: ['./person-details.page.scss'],
})
export class PersonDetailsPage implements OnInit {

  person

  constructor(
    private activeRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.activeRoute.queryParamMap.subscribe(params => {
      if(this.router.getCurrentNavigation().extras.state) {
        this.person = this.router.getCurrentNavigation().extras.state.person
        console.log(this.person)
      }
    })
  }

}
