import { Component } from '@angular/core';
import { NavigationExtras } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import { CreateComponent } from '../components/modals/create/create.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  persons = [
    {
      name: "Edmar Diaz"
    },
    {
      name: "Juan Dela Cruz"
    },
  ]

  constructor(
    private modalCtrl: ModalController,
    private navCtrl: NavController
  ) {}


  async presentModal() {

    const modal = await this.modalCtrl.create({
      component: CreateComponent
    })

    modal.onWillDismiss().then((data) => {
      console.log(data)
      if(data.data) {
        this.persons.push(data.data.person)
      }
    })

    return await modal.present()

    // let person = {
    //   name: "Jose"
    // }
    // this.persons.push(person)
  }

  delete(index) {
    this.persons.splice(index, 1)
  }

  details(person) {
    let navigationExtras: NavigationExtras = {
      state: {person: person}
    }

    this.navCtrl.navigateForward('/person-details', navigationExtras)
  }
}
